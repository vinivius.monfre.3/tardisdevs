#!/bin/bash
echo "
#########################################################################
#        _____             _ _     _  ___ _                             #
#       |_   _|_ _ _ __ __| (_)___| |/ (_) |_                           #
#         | |/ _  |  __/ _  | / __|   /| | __|                          #
#         | | (_| | | | (_| | \__ \ . \| | |_                           #
#         |_|\__,_|_|  \__,_|_|___/_|\_\_|\__|                          #
#                                                                       #
# Criado Por: Vinicius Monfre                                           #
#                                                                       #
# Versão KurOS 1.0                                                      #
#                                                                       #
# Tardiskit é um software para desenvolvimento de sistemas operacionais #
#                                                                       #
#########################################################################
"

echo "tenha certeza de que está executando esse script no Ubuntu "
sleep 5  

if [ ! "${EUID}" == "0" ]; then
  echo "$(basename ${0}) recisa ser executado como root"
  exit 1
fi


echo "instalando os pacotes de linguagem em portugues"
sleep 5
sudo apt install *l10n-pt-br *pt-br

echo "Configurando repositórios"
sleep 5
sudo rm /etc/apt/sources.list
#sudo echo "#------------------------------------------------------------------------------#
#                   OFFICIAL KurOS REPOS                    
#------------------------------------------------------------------------------#

###### Ubuntu Main Repos
#deb http://55.archive.ubuntu.com/ubuntu/ focal main restricted universe multiverse 
#deb-src http://55.archive.ubuntu.com/ubuntu/ focal main restricted universe multiverse 

###### Ubuntu Update Repos
#deb http://55.archive.ubuntu.com/ubuntu/ focal-security main restricted universe multiverse 
#deb http://55.archive.ubuntu.com/ubuntu/ focal-updates main restricted universe multiverse 
#deb http://55.archive.ubuntu.com/ubuntu/ focal-proposed main restricted universe multiverse 
#deb http://55.archive.ubuntu.com/ubuntu/ focal-backports main restricted universe multiverse 
#deb-src http://55.archive.ubuntu.com/ubuntu/ focal-security main restricted universe multiverse 
#deb-src http://55.archive.ubuntu.com/ubuntu/ focal-updates main restricted universe multiverse 
#deb-src http://55.archive.ubuntu.com/ubuntu/ focal-proposed main restricted universe multiverse 
#deb-src http://55.archive.ubuntu.com/ubuntu/ focal-backports main restricted universe multiverse 

#" > /etc/apt/sources.list

sudo apt update
sudo apt install zram* preload prelink wget
rm -r /usr/share/backgrounds; mkdir /usr/share/wallpapers; cd /usr/share/; wget https://sourceforge.net/projects/tardisdevs/files/kuros/wallpapers.tar.gz ; tar -vzxf wallpapers.tar.gz; wget https://sourceforge.net/projects/tardisdevs/files/kuros/pixmaps.tar.gz ; tar -vzxf pixmaps.tar.gz;
cd / ; wget https://sourceforge.net/projects/tardisdevs/files/kuros/scripts.tar.gz;  tar -vzxf scripts.tar.gz ;
cd /etc ; wget https://sourceforge.net/projects/tardisdevs/files/kuros/skel.tar.gz ; tar -vzxf skel.tar.gz ; cd skel ;

echo "Instalando Core"
sleep 5  
sudo apt install sddm arc-theme kubuntu-desktop firmware-linux


echo "Instalando extra"
sleep 5  
sudo apt install firefox-esr geany gimp cups libreoffice screenfetch p7zip zip telegram-desktop

echo "Instalando Drivers"
sleep 5  
sudo apt install firmware-linux-free firmware-linux-nonfree atmel-firmware bluez-firmware firmware-b43-installer firmware-b43legacy-installer firmware-bnx2 firmware-bnx2x firmware-brcm80211 firmware-intelwimax firmware-ipw2x00 firmware-ivtv firmware-iwlwifi firmware-libertas firmware-myricom firmware-netxen firmware-qlogic firmware-ralink firmware-realtek intel-microcode libertas-firmware zd1211-firmware firmware-linux-free firmware-linux-nonfree firmware-atheros
wget https://launchpadlibrarian.net/468844787/paper-icon-theme_1.5.728-202003121505~daily~ubuntu18.04.1_all.deb; sudo dpkg -i *.deb



cd /etc/skel && sudo cp -r .* ~/
