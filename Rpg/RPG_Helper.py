# Imports
import discord
import asyncio
import random
import os
from discord.ext import commands

TOKEN = 'token'

client = commands.Bot(command_prefix='!')

@client.event
async def on_ready():
    print('Connected to bot: {}'.format(client.user.name))
    print('Bot ID: {}'.format(client.user.id))

@client.command()
async def Oi (ctx):
    await ctx.send('Olá. eu sou o sistema GCT. como POSSO ajudar?')
    await ctx.send('!d numero: roda um dado do valor escolhido, !criatura: Invoca um numero de criaturas')
    await ctx.send('!loot: Resgata Loot, !acontece: Gera acontecimentos aleatórios, !loja: abre a loja')
    await ctx.send('!iniciativa: Rola Um Teste De Iniciativa, !sorte: Roda um teste de sorte, !umi: roda um teste especifico para a umi')
    await ctx.send('!responde + pergunta: responde uma pergunta, !ficha: envia o modelo da ficha, !salva: salva uma ficha, !minha: imprime uma ficha')

@client.command()
async def d (ctx):
    if (ctx.author.id == 'ID'): return   
    n = ctx.message.content[2:].strip()
    numr = random.randint(1,int(n))
    await ctx.send(numr)


@client.command()
async def criatura (ctx):
    numr = random.randint(1,20)
    await ctx.send('surgem')
    await ctx.send(numr)
    await ctx.send('criaturas')


@client.command()
async def loot (ctx):
    numr = random.randint(1,20)
    nnhu = random.randint(1,500)
    await ctx.send('Você encontrou ')
    await ctx.send(nnhu)
    await ctx.send(' reais')
    if numr < 3:
        await ctx.send('Você encontrou uma arma de fogo')
    elif numr < 5:
        await ctx.send('Você encontrou uma faca enferrujada')
    elif numr < 10:
        await ctx.send('Você encontrou um fação')
    elif numr < 15:
        await ctx.send('Você encontrou um pé de cabra')
    elif numr < 18:
        await ctx.send('Você encontrou uma haste de ferro afiada')
    elif numr == 18:
        await ctx.send('Você encontrou uma barra de metal')
    elif numr == 19:
        await ctx.send('Você encontrou um bastão de madeira')
    else:
        await ctx.send('Rode o comando !criatura')
        
        
@client.command()
async def acontece (ctx):
    numr = random.randint(1,20)
    if numr == 1:
        await ctx.send('você achou uma bateria de radio amador caida na areia ')
    elif numr == 2:
        await ctx.send('Você estava andando tranquilamente quando tropeça em um kit médico parcialmente enterrado e ele tem ')
        numr = random.randint(1,3)
        await ctx.send(numr)
        await ctx.send(' usos')        
    elif numr == 3:
        await ctx.send('Você encontrou uma lata de comida')
    elif numr == 4:
        await ctx.send('Você encontrou um isqueiro')
    elif numr == 5:
        await ctx.send('Você encontrou um kit de primeiros socorros')
    elif numr == 6:
        await ctx.send('Você encontrou um maço de cigarro com ')
        numr = random.randint(1,10)
        await ctx.send(numr)
        await ctx.send(' cigarros') 
    elif numr == 7:
        await ctx.send('Você encontrou um caderno')
    elif numr == 8:
        await ctx.send('Você encontrou uma garrafa de agua')
    elif numr == 9:
        await ctx.send('Você estava andando tranquilamente quando encontra...')
        await ctx.send('quando encontra...')
        await ctx.send('o...')
        await ctx.send('DEMONIO')
    elif numr == 10:
        await ctx.send('Você Desenvolveu desinteria') 
    elif numr == 11:
        await ctx.send('Você sente dor de cabeça')   
    elif numr == 12:
        await ctx.send('Você encontrou comprimido de aspirina')
    elif numr ==13:
        await ctx.send('o engenheiro acaba de ter um derrame')
    elif numr <15:
        await ctx.send('Você estava andando de boas quando um pedaço de um radiozumbi enrosca no seu pé. e ele quer um novo hospedeiro.')
        await ctx.send('rode o comando !iniciativa')
    elif numr < 18:
        await ctx.send('Você tropeçou e caiu')
    elif numr == 18:
        await ctx.send('Você estava andando junto de seus aliados quando escuta um barulho de rachadura e todos que falharem no teste de destreza caem em um estacionamento subterraneo com o comando !criatura')
    elif numr == 19:
        await ctx.send('Você leva ')
        numr = random.randint(1,4)
        await ctx.send(numr)
        await ctx.send(' de dano para o cerne que te mordeu de surpresa')
    else:
        await ctx.send('Rode o comando !criatura')     
@client.command()
async def iniciativa (ctx):
    Init = ['thiago','UMI', 'davi', 'kevin', 'isaque', 'wesley', 'douglas', 'mestre']
    random.shuffle(Init)
    await ctx.send(Init)


@client.command()
async def sorte (ctx):
    numr = random.randint(1,20)
    if numr < 6:
        nnhu = random.randint(1,50)
        await ctx.send('Você encontrou ')
        await ctx.send(nnhu)
        await ctx.send(' reais')
        await ctx.send('Você Está com sorte')
    elif numr < 15:
        nnhu = random.randint(1,50)
        await ctx.send('Você perdeu ')
        await ctx.send(nnhu)
        await ctx.send(' reais')
        await ctx.send('você está com azar')
    else:
        await ctx.send('Aconteceu uma desgraça')
        
        
        
@client.command()
async def loja (ctx):
    await ctx.send('Seja Bem-Vindo a loja')
    await ctx.send('Tabela 1 - Malas \n Mala de viagem média(5KG) 2,5 KG R$ 100,00 \n mala de viagem grande (20kg) 5kg R$ 150,00 \n Mala de viagem enorme(40KG) 7,5 kg R$ 200,00')
    await ctx.send('Tabela 2 - Roupas \n Camiseta R$ 150,00 \n Camisa R$ 150,00 \n casaco R$ 150,00 \n Sobretudo R$ 200,00 \n colete R$ 100,00 \n cinto de ferramentas R$ 50,00 \n terno R$ 500,00 \n shorts R$ 90,00 \n calça R$ 268,00')
    await ctx.send('Tabela 3 - Eletronicos e computadores \n Camera Fotografica 1KG R$ 500,00 \n Celular R$ 500,00 \n Computador Desktop 5KG R$ 2500,00 \n Notebook 2.5KG R$ 3000,00 \n Upgrade de PC 5KG 2000,00 \n gravador de som digital 0.5KG R$ 150,00 \n Impressora  multifuncional 1.5KG R$ 300,00 \n radio amador a bateria 0.5 KG R$ 200,00')    
    await ctx.send('Tabela 4 - Equipamentos de segurança \n Mascara para identificador de chamada 0.5 KG R$ 70,00 \n Interceptador de celular 0.5KG R$ 3000,00 \n Detector de metais 1KG R$ 230,00 \n Óculos de visão noturna 1,5KG R$ 300,00 \n Rastreador de ligação 2.5KG R$ 3000,00')
    await ctx.send('Tabela 5 - Equipamento profissional \n cortador de trava 2,5KG R$ 36,00 \n Kit para arrombamento de carros 0.5KG R$ 36,00 \n Kit de química 3KG R$ 400,00 \n Kit de demolição 2,5KG R$ 450,00 \n Kit de camuflagem 2.5KG R$ 60,00 \n kit de eletrônica básico 6KG R$ 900,00 \n Kit de primeiros socorros 3KG R$ 300,00 \n Kit de falsificação 1,5KG R$ 600,00 \n Kit de arrombador 0,5KG R$ 120,00 \n Kit de mecânica básico 2,5KG R$ 100,00 \n Kit farmaceutico 3KG R$ 500,00 \n Kit de resgate 3,5KG R$400,00 \n kit cirurgico 2,5KG R$300')
    await ctx.send('Tabela 6 - Equipamento de sobrevivencia \n binoculos 1.5KG R$200,00 \n lanterna 0.5KG R$20,00 \n máscara de gás 2.5KG R$100,00 \n saco de dormir 2KG R$200,00 \n tenda para 2 pessoas 2KG R$ 500,00')
    await ctx.send('Tabela 7 - Alimentos \n 1 duzia de ovos 1KG R$10,00 \n bife 1KG R$ 50,00 \n massa pronta para tapioca 1KG R$30,00 \n Refeição para 1 pessoa 1KG R$ 20,00, agua 1l 1KG R$ 14,00 \n cigarro maço com 20 0.5KG R$ 30,00 \n isqueiro zippo R$ 80,00')
    await ctx.send('Tabela 8 - Armas leves \n Revolver 2d6 perfurante, alcance 100m. 8 disparos por pente R$2950,00 1KG \n pistola automatica e semi 2d6 perfurante. alcance 50m. 15 disparos por pente R$5420,00 2KG \n espingarda de 2 mãos chumbo grosso 2 disparos por vez, 2d12 perfurante, alcance 50m, 4.5KG R$5648,00')
    await ctx.send('Tabela 9 - Armas pesadas \n fuzil de assalto 2d8 perfurante, alcance 300m, 30 disparos por pente 3.5kg R$8325,00 \n metralhadora 2d8 perfurante, alcance 300m, 100 balas por pente, 7.5KG R$ 12500,00')   
    await ctx.send('Tabela 10 - Munições \n 1 pente de munição de pistola 1kg R$300,00 \n pacote com 10 balas de espingarda 1KG R$600,00 \n 1 pente de munição de fuzil 2KG R$ 900,00 \n 1 pente de munição de metralhadora 5KG R$ 1000,00')


@client.command()
async def responde (ctx):
    resposta = random.choice(['Te vira ae filhão','Sim','As vezes','Não','Claro','NUNCA!','Um dia talvez','A resposta está dentro de ti','Mais ou menos','Uma Bosta','Podia ser pior'])
    await ctx.send(resposta)

@client.command()
async def umi (ctx):
    if (ctx.author.id == 'ID'): return   
    n = ctx.message.content[4:].strip()
    numr = random.randint(1,int(n))
    umi = random.randint(1,int(n))
    if numr < umi:
        await ctx.send(numr)
    else:
        await ctx.send(umi)
        
        
@client.command()
async def ficha (ctx):
    await ctx.send('Ficha de personagem \n \n \n Nome Do Personagem: \n Jogador:\n Raça: \n Habilidades\n -------------------------------- \n Força: \n Destreza: \n Constituição: \n Inteligência: \n Sabedoria: \n Carisma: \n  -------------------------------- \n HP:\n MP: \n  --------------------------------\n Perícias \n  -------------------------------- \n Acrobacia: \n Atletismo: \n Atuação: \n Cavalgar: \n Conhecimento*: \n Cura: \n Diplomacia: \n Enganar: \n Fortitude: \n Furtividade: \n Iniciativa: \n Intimidar: \n Intuição: \n Luta/Briga: \n Magia*: \n Percepção: \n Pilotar: \n Mira: \n Reflexos: \n Religião: \n Sobrevivência: \n Vontade: \n  -------------------------------- \n Defesa: \n Proficiências: \n Armaduras e escudos: \n Tamanho: \n XP: \n Deslocamento: \n  -------------------------------- \n Mochila: \n  -------------------------------- \n Magias: \n  -------------------------------- \n Anotações: \n  -------------------------------- \n ')        
        
         
@client.command()
async def salvar(ctx):
    if (ctx.author.id == 'ID'): return   
    msg = ctx.message.content[7:].strip()
    await ctx.send('salvo')
    arquivo = open(f"{ctx.message.author.name}.txt", "a")
    arquivo.write(f"ficha de {ctx.message.author.name} \n")
    arquivo.write(msg)
    
    
         
@client.command()
async def minha(ctx):
    #arquivo = open(f"{ctx.message.author.name}.txt", "r")
    #await ctx.send(arquivo.readlines())
    with open(f"{ctx.message.author.name}.txt", "rb") as file:
        await ctx.send("Sua ficha é", file=discord.File(file, f"ficha-{ctx.message.author.name}.txt"))
    
@client.command()
async def overclock(ctx):
    numr = random.randint(1,200)
    await ctx.send("seu limitador é")
    await ctx.send(numr)
    limitador = numr
    total = 0
    iterador = [1,2,3,4,5,6,7,8,9,0]
    n = 0
    for n in iterador:
        numr = random.randint(1,20)
        total = total + numr
    await ctx.send("Você tirou ")
    await ctx.send(total)
    if total < limitador/10 :
        await ctx.send("foi um extremo")
    elif total < limitador/2:
        await ctx.send("foi bom")
    elif total < limitador :
        await ctx.send("foi um normal")
    else :
        await ctx.send("seu braço explode")
        await ctx.send(" e você toma")
        numr = random.randint(1,15)
        await ctx.send(numr)
        await ctx.send("de dano")
    
@client.command()
async def jordi (ctx):
    resposta = random.choice(['Não fui programado para isso', 'achei um !loot','Sim','As vezes','Não','Claro','NUNCA!','encontrei um !criatura','Siga seu coração','Mais ou menos','opa. to fora de alcance','Me derrubaro aqui ó', ' rode um !acontece', 'agora são !d 12 horas e !d60 minutos' , 'vê com o mestre ae', ' rola um teste de sorte', 'encontrei resquicios do demonio', 'plim plim plim. uma ameaça foi detectada. rode um !criatura', ' não tem nada aí seu tolo', 'olha. achei um cranifago. rode um !iniciativa'])
    await ctx.send(resposta)
    
    
    
client.run(TOKEN)
			
