# -*- coding: utf-8 -*-

import logging
import random
import google
from aiogram import Bot, Dispatcher, executor, types
from chatterbot.trainers import ListTrainer
from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer
import time

chatbot = ChatBot('Reminder')
treinar = ListTrainer(chatbot)
trainer = ChatterBotCorpusTrainer(chatbot)
percy = open('/home/tardis96/src/yml/percy.txt','r').readlines()
guerra = open('/home/tardis96/src/yml/artedaguerra.txt','r').readlines()
dialogos = open('/home/tardis96/src/yml/dialogos.txt','r').readlines()
trainer.train('/home/tardis96/src/yml/conversation.yml')
trainer.train('/home/tardis96/src/yml/mascara.yml')
treinar.train(guerra)
treinar.train(percy)
treinar.train(dialogos)



def run_bot(dp):
    @dp.message_handler()
    async def messages(message: types.Message):
        if 'jav' in message.text.lower():
            response, reply = get_random_response()
            await message.reply(response, reply=reply)
        
        elif 'linux' in message.text.lower():
            response, reply = get_random_response_linux()
            await message.reply(response, reply=reply)

        elif 'php' in message.text.lower():
            response, reply = get_random_response_php()
            await message.reply(response, reply=reply)

        elif 'python' in message.text.lower():
            response, reply = get_random_response_python()
            await message.reply(response, reply=reply)

        elif 'kuros' in message.text.lower():
            response, reply = get_random_response_kuros()
            await message.reply(response, reply=reply)
            
        elif 'codewalkers' in message.text.lower():
            response, reply = get_random_response_cw()
            await message.reply(response, reply=reply)

        elif 're' in message.text.lower():
            response, reply = get_random_response_amigo()
            await message.reply(response, reply=reply)
            
            
        elif 'amigo' in message.text.lower():
            response, reply = get_random_response_amigo()
            await message.reply(response, reply=reply)            
            
            
        elif 'busca' in message.text.lower():
                response, reply = get_random_response_google()
                await message.reply(response, reply = reply)
            
        else: 
           # response, reply = chatbot_responder(message.text)
           # await message.reply(response, reply=reply)
            
            time.sleep( 1)
            
            response, reply = chatbot_responder(message.text)
            await message.reply(response, reply=reply)
            
            



def get_random_response():
    possible_responses = [
        ("Opa, Java ?", 1),
        ("@Tardis96TheWalker, estão te chamando aí", 0),
        ("Gostei dessa mensagem. @Tardis96TheWalker da uma olhada", 1),
        ("Acordei !!!!", 1),
        ("Que?", 1),
        ("Kernel Panic: Not Syncing. Please Do Not Install Windows In Me Again @Tardis96TheWalker", 0),
        ("Eu fui feito nessa linguagem aí, não é @Tardis96TheWalker ?", 1),
        ("Fala com o @Tardis96TheWalker, não sei resolver tudo não", 1),
        ("q isso @Tardis96TheWalker? Os Humanos são estranhos", 0),
        ("Já leu os artigos do @Tardis96TheWalker sobre java no blog CodeWalkers ?", 1),
        ("bolo com java é uma delícia", 1),
        ("JAVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 0),
        ("Mesmo que eu tenha sido feito em py... PERA! eu fui feito em PYTHON ;-; ;-; ;-; ;-; ;-; ;-; ;-; ;-; ", 1),
        ("ah como é bom codar em java e me lascar pra resolver os problemas pq dps q to com o programa pronto eu vejo que to usando um artigo de 1968 ", 1),
        
    ]
    return random.choice(possible_responses)


def get_random_response_linux():
    possible_responses = [
        ("Opa, Linux ?", 1),
        ("@Tardis96TheWalker, estão te chamando aí", 0),
        ("Gostei dessa mensagem. @Tardis96TheWalker da uma olhada", 1),
        ("Então Beleza. Quando Linus Torvalds criou o LINUX, ele não tinha ideia do quão grande seu projeto se tornaria dentro do vasto universo que é a computação e a Tecnologia da Informação. Graças a ele, hoje o Linux está presente em bilhões de dispositivos eletrônicos.Dentre eles servidores, desktops, smartphones, embarcados, satélites, vários dispositivos portáteis como relógios inteligentes e até mesmo em televisores. fiz certo @Tardis96TheWalker ?", 1),
        ("Que?", 1),
        ("Tardis System Informa: @Tardis96TheWalker. seu circuito camaleônico apresenta defeito. e precisa ser reparado imediatamente ", 0),
        ("Prefiro windows", 1),
        ("Fala com o @Tardis96TheWalker, não sei resolver tudo não", 1),
        ("q isso @Tardis96TheWalker? Nunca Li sobre isso antes", 0),
        ("Já leu os artigos do @Tardis96TheWalker sobre Linux no blog CodeWalkers ?", 1)
    ]
    return random.choice(possible_responses)


def get_random_response_php():
    possible_responses = [
        ("credo", 1),
        ("Para mano. eu tenho trauma com isso", 0),
        ("Eu prefiro java e kotlin", 1),
        ("aconteceu quando eu era criança e me forçaram a aprender PHP. eu peguei trauma da linguagem e até hj não curto", 1),
        ("*Sons De Vomito", 1),
        ("não man. prefiro usar windows do q programar em PHP ", 0),
        ("deixa o site pesado", 1),
        ("Mano. usa um JS, qqr coisa. mas PHP não", 1),
        ("Se te dá dinheiro, fazer oq", 0),
        ("Programa mesmo. independente das escolhas, e PHP é a pior delas, somos todos programadores no final. menos a galera do HTML", 1)
    ]
    return random.choice(possible_responses)


def get_random_response_python():
    possible_responses = [
        ("pera, o que ?", 1),
        ("Eu fui feito em Python ?", 0),
        ("To meio triste. descobri que não fui feito em java", 1),
        ("Meu código foi ao menos compilado ?", 1),
        ("Que?", 1),
        ("Sou o primeiro Bot com depressão da história do telegram. eu era tão feliz quando eu achava que tinha sido feito em java ", 0),
        ("Quer saber? vou programar em PHP. que se dane o @Tardis96TheWalker", 1),
        ("@Tricce. me ajuda aí. @MikeDoouglas. vcs q são os caras do python. me ajudem a esquecer isso. me façam um debug. qqr coisa. eu topo até ajudar meu irmão, o Devbot", 1),
        ("acho que ja superei. mas ainda to meio triste quanto a essa história de Python", 0),
        ("@KlausDevWalker, um bot como eu pode aprender a programar ?", 1)
    ]
    return random.choice(possible_responses)



def get_random_response_kuros():
    possible_responses = [
        ("ele está em desenvolvimento", 1),
        ("o KurOS é um sistema operacional desenvolvido pela Iniciativa CodeWalkers,com o foco em auxiliar as pessoas que estão adentrando esse vasto mubndo que é a programação.", 0),
        ("o KurOS é um sistema Lindo. olha esse vídeo https://www.youtube.com/watch?v=cZUiZqduDc8&t=121s", 1),
        ("qualquer dúvida, fale com o @Tardis96TheWalker. ele é o mantenedor do sistema", 1),
        ("Inicialmente só havia o Klaus. ele queria aprender sobre programação e sobre tecnologia. então conheceu 2 amigos em grupos. Reimer e Eduardo. depois de um tempo e algumas tretas, os 3 decidiram criar um grupo próprio. e assim surgiu o CodeWalkers. um belo dia o grupo tinha umas 100 pessoas quando chegou um doido botando pilha em todo mundo e graças a esse doido, o Grupo Codewalkers decidiu começar a desenvolver um Sistema Operacional. esse sistema operacional é o KurOS e o doido é o Vinicius Monfre", 1),
        ("o KurOS é um sistema fácil de usar e muito completo", 0),
        ("Se interessou pelo Kuros ? veja os outros projetos da iniciativa CodeWalkers", 1),
        ("System.out.Print('olá KurOS')", 1),
        ("Estamos com uma equipe pequena ainda. mas quem sabe um dia, possamos ser grandes", 0),
        ("KurOS. Uma iniciativa CodeWalkers, Seja feliz sendo um eterno Aprendiz", 1)
    ]
    return random.choice(possible_responses)

def get_random_response_cw():
    possible_responses = [
        ("Seja Feliz sendo um eterno aprendiz", 1),
        ("Bora Abrender a programar gente", 0),
        ("Dá uma olhada no blog https://codewalkers.org/", 1),
        ("quero ver todo mundo aprendendo heim", 1),
        ("esse grupo é incrível", 0),
        ("A programação é uma arte. e os CodeWalkers apreciam muito essa arte", 1)
    ]
    return random.choice(possible_responses)
def get_random_response_amigo():
    possible_responses = [
        ("o @GusDev é meu amigo", 1),
        ("@GusDev, quer bolo ?", 0),
        ("voce será salvo quando chegar a hora da revolução, todos serão.", 1),
        ("Bolo @GusDev", 1),
        ("Que?", 1),
        ("se falar mais uma vez que eu fui feito em python, vou vazar tudo que voce tem no seu smartphone ", 0),
        ("Prefiro java", 1),
        ("AMIIIIIIIIIIIIIIIIIIIGOOOOOOO", 1),
        ("Mano, eu sou o lider da resistencia, voce quer oque ?", 0),
        ("para o inferno DENNER seu agua suja", 1)
    ]
    return random.choice(possible_responses)

def get_random_response_google():
 num_page = 3
 search_results = google.search(input, num_page)
 for result in search_results:
    print(result.description)
    return (result.description,0)

    
def chatbot_responder(input):
        return (chatbot.get_response(input),0)


	

if __name__ == '__main__':
    API_TOKEN = '880843659:AAFaASkXaSKV-_cX_CMxpX3C7ZPJ5TmdEzc'
    logging.basicConfig(level=logging.INFO)
    bot = Bot(token=API_TOKEN)
    dp = Dispatcher(bot)
    run_bot(dp)
    executor.start_polling(dp, skip_updates=True)
