# -*- coding: utf-8 -*-

import logging
import random
from aiogram import Bot, Dispatcher, executor, types
import time
import os
import subprocess
#from subprocess import Popen
youtube = ""

def run_bot(dp):
    @dp.message_handler()
    async def messages(message: types.Message):
        global youtube
        youtube = message.text
        if 'play' in message.text.lower():
            response, reply = get_random_response()
            await message.reply(response, reply=reply)
        elif 'stop' in message.text.lower():
            response, reply = get_random_pare()
            await message.reply(response, reply=reply)
        elif 'list' in message.text.lower():
            response, reply = get_random_list()
            await message.reply(response, reply=reply)
        elif 'run' in message.text.lower():
            response, reply = get_random_run()
            await message.reply(response, reply=reply)
        elif 'delete' in message.text.lower():
            response, reply = get_random_delete()
            await message.reply(response, reply=reply)
        elif 'random' in message.text.lower():
            response, reply = get_random_rand()
            await message.reply(response, reply=reply)
        elif 'skip' in message.text.lower():
            response, reply = get_random_skip()
            await message.reply(response, reply=reply)
        elif 'last' in message.text.lower():
            response, reply = get_random_prev()
            await message.reply(response, reply=reply)
        
def get_random_response():
    possible_responses = [
        ("👍", 0), 
        ("TAPÃO",1),
        ("COMEMORAÇÃO",0),
        ("PÓ",1),
    ]
    yt = youtube [5:]
    print (yt)
    arquivo = open("musica.sh", "w")
    
    frases = list()
    frases.append("#!/bin/bash \n")
    frases.append("mpv --no-video ")
    frases.append(yt)
    arquivo.writelines(frases)
    command = './musica.sh'
    subprocess.Popen(command, shell=True)
    #os.system("sh musica.sh")
    return random.choice(possible_responses)
    

def get_random_pare():
    possible_responses = [
        ("👎", 0), 
        ("DISGRAÇA, minha musica",1),
        ("CAPETA, eu tava curtindo",0),
        ("OPORRAA",1),
    ]


    command = 'killall mpv'
    subprocess.Popen(command, shell=True)
    return random.choice(possible_responses)


def get_random_list():
    possible_responses = [
        ("adicionado a playlist", 0), 
        ("adicionado a fila",1),
    ]
    yt = youtube [5:]
    print (yt)
    arquivo = open("lista.txt", "a")
    
    frases = list()
    frases.append(yt)
    frases.append( "\n")
    arquivo.writelines(frases)
    #os.system("sh musica.sh")
    return random.choice(possible_responses)
    

def get_random_run():
    possible_responses = [
        ("👍", 0), 
        ("TAPÃO",1),
        ("COMEMORAÇÃO",0),
        ("PÓ",1),
    ]
    command = 'mpv --no-video --playlist=lista.txt --loop-playlist'
    subprocess.Popen(command, shell=True)
    #os.system("sh musica.sh")
    return random.choice(possible_responses) 

def get_random_delete():
    possible_responses = [
        ("playlist deletada", 0), 
        ("playlist removida",1),
    ]
    yt = youtube [5:]
    print (yt)
    arquivo = open("lista.txt", "w")
    
    frases = list()
    frases.append(" ")
    frases.append( "")
    arquivo.writelines(frases)
    return random.choice(possible_responses)


def get_random_rand():
    possible_responses = [
        ("👍", 0), 
        ("TAPÃO",1),
        ("COMEMORAÇÃO",0),
        ("PÓ",1),
    ]
    command = 'mpv --shuffle --no-video --playlist=lista.txt --loop-playlist'
    subprocess.Popen(command, shell=True)
    #os.system("sh musica.sh")
    return random.choice(possible_responses)

def get_random_skip():
    possible_responses = [
        ("👎", 0), 
        ("DISGRAÇA, minha musica",1),
        ("CAPETA, eu tava curtindo",0),
        ("OPORRAA",1),
    ]


    command = 'echo playlist-next | socat - "/home/tardis96/.config/mpv/socket"'
    subprocess.Popen(command, shell=True)
    return random.choice(possible_responses)


def get_random_prev():
    possible_responses = [
        ("👎", 0), 
        ("DISGRAÇA, minha musica",1),
        ("CAPETA, eu tava curtindo",0),
        ("OPORRAA",1),
    ]


    command = 'echo playlist-prev | socat - "/home/tardis96/.config/mpv/socket"'
    subprocess.Popen(command, shell=True)
    return random.choice(possible_responses)


if __name__ == '__main__':
    API_TOKEN = 'SEU TOKEN'
    logging.basicConfig(level=logging.INFO)
    bot = Bot(token=API_TOKEN)
    dp = Dispatcher(bot)
    run_bot(dp)
    executor.start_polling(dp, skip_updates=True)
